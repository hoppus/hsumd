from django.shortcuts import render
from django.views import generic

from .models import ImageCollection


class ImageCollectionView(generic.ListView):
    model = ImageCollection