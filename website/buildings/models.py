from django.db import models


class ImageCollection(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Image(models.Model):
    name = models.CharField(max_length=30)
    caption = models.CharField(max_length=300)
    url = models.URLField(max_length=200)
    image_collection = models.ForeignKey(ImageCollection, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
