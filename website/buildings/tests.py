from django.test import TestCase
from django.urls import reverse

from buildings.models import Image, ImageCollection


class BuildingsModelTest(TestCase):

    def test_saving_and_retrieving_image_collection(self):
        image_collection = ImageCollection()
        image_collection.name = 'Clock Tower'
        image_collection.save()

        image_collections = ImageCollection.objects.all()
        self.assertEqual(image_collections.count(), 1)

        self.assertEqual(image_collections[0].name, 'Clock Tower')

    def test_saving_and_retrieving_image(self):
        image_collection = ImageCollection()
        image_collection.name = 'Clock Tower'
        image_collection.save()
        building_image_collection = ImageCollection.objects.all()[0]

        building_image = Image()
        building_image.name = 'Clock Tower Aftermath'
        building_image.caption = 'Struck by lightning in 1955'
        building_image.url = 'http://hillvalley.org/clock-tower1.png'
        building_image.image_collection = building_image_collection
        building_image.save()

        self.assertEqual(building_image.name, 'Clock Tower Aftermath')
        self.assertEqual(building_image.caption, 'Struck by lightning in 1955')
        self.assertEqual(building_image.url, 'http://hillvalley.org/clock-tower1.png')
        self.assertEqual(building_image.image_collection.name, 'Clock Tower')


class BuildingsViewTest(TestCase):

    def test_uses_buildings_index_template(self):
        response = self.client.get(reverse('buildings'))
        self.assertTemplateUsed(response, 'buildings/imagecollection_list.html')

    def test_displays_all_items(self):
        image_collection = ImageCollection()
        image_collection.name = 'Clock Tower'
        image_collection.save()
        building_image_collection = ImageCollection.objects.all()[0]

        building_image = Image()
        building_image.name = 'Clock Tower Aftermath'
        building_image.caption = 'Struck by lightning in 1955'
        building_image.url = 'http://hillvalley.org/clock-tower1.png'
        building_image.image_collection = building_image_collection
        building_image.save()

        response = self.client.get(reverse('buildings'))
        self.assertContains(response, 'Clock Tower')
        self.assertContains(response, 'Struck by lightning in 1955')