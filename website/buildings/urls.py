from django.urls import path

from . import views

urlpatterns = [
    path('', views.ImageCollectionView.as_view(), name='buildings'),
]