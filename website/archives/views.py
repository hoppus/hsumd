from django.db.models import Q
from django.shortcuts import render
from django.views import generic

from .models import Collection, Map, Video



class CollectionView(generic.ListView):
    model = Collection

class MapListView(generic.ListView):
    model = Map
    paginate_by = 10

    def get_queryset(self):
        keywords = self.request.GET.get('q')
        qs = Map.objects.all()
        if keywords is not None:
            qs = qs.filter(
                Q(title__icontains=keywords) |
                Q(map_type__icontains=keywords) |
                Q(location__icontains=keywords) |
                Q(notes__icontains=keywords)
            )

        return qs

class MapDetailView(generic.DetailView):
    model = Map

class VideoListView(generic.ListView):
    model = Video
    paginate_by = 10

    def get_queryset(self):
        keywords = self.request.GET.get('q')
        qs = Video.objects.all()
        if keywords is not None:
            qs = qs.filter(
                Q(title__icontains=keywords) |
                Q(map_description__icontains=keywords)
            )

        return qs

class VideoDetailView(generic.DetailView):
    model = Video
