from django.contrib import admin

from .models import Collection, Map, Video


admin.site.register(Collection)
admin.site.register(Map)
admin.site.register(Video)