from django.urls import path
from . import views


urlpatterns = [
    path('', views.CollectionView.as_view(), name='archives'),
    path('maps/', views.MapListView.as_view(), name='maps'),
    path(
        'maps/<int:pk>', 
        views.MapDetailView.as_view(), 
        name='map-detail'),
    path('videos/', views.VideoListView.as_view(), name='videos'),
    path(
        'videos/<int:pk>',
        views.VideoDetailView.as_view(),
        name='video-detail'),
]
