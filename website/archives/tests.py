from datetime import datetime

from django.test import TestCase

from archives.models import Collection, Map, Video


class collectionModelTest(TestCase):

    def test_saving_and_retrieving_collection(self):
        some_collection = Collection()
        some_collection.title = 'Some collection'
        some_collection.description = 'This is just a test collection'
        some_collection.image = 'http://mcfly.org/mcfly-home.png'
        some_collection.url = 'things'
        some_collection.save()

        saved_collections = Collection.objects.all()
        self.assertEqual(saved_collections.count(), 1)

        self.assertEqual(some_collection.title, 'Some collection')
        self.assertEqual(some_collection.description, 'This is just a test collection')
        self.assertEqual(some_collection.image, 'http://mcfly.org/mcfly-home.png')
        self.assertEqual(some_collection.url, 'things')


class MapModelTest(TestCase):

    def test_saving_and_retrieving_map(self):
        first_map = Map()
        first_map.map_type = 'topographical'
        first_map.title = 'first map'
        first_map.location = 'Ridgecrest, CA'
        first_map.origin_date = 1998
        first_map.number_of_copies = 182
        first_map.notes = 'Welcome to the desert.'
        first_map.save()

        second_map = Map()
        second_map.map_type = 'nontopographical'
        second_map.title = 'second map'
        second_map.location = 'Tehachapi, CA'
        second_map.origin_date = 2009
        second_map.number_of_copies = 42
        second_map.notes = 'It is a bit cooler here.'
        second_map.save()

        saved_maps = Map.objects.all()
        self.assertEqual(saved_maps.count(), 2)

        first_saved_map = saved_maps[0]
        self.assertEqual(first_saved_map.map_type, 'topographical')
        self.assertEqual(first_map.title, 'first map')
        self.assertEqual(first_map.location, 'Ridgecrest, CA')
        self.assertEqual(first_map.origin_date, 1998)
        self.assertEqual(first_map.number_of_copies ,182)
        self.assertEqual(first_map.notes, 'Welcome to the desert.')

        second_saved_map = saved_maps[1]
        self.assertEqual(second_saved_map.map_type, 'nontopographical')
        self.assertEqual(second_map.title, 'second map')
        self.assertEqual(second_map.location, 'Tehachapi, CA')
        self.assertEqual(second_map.origin_date, 2009)
        self.assertEqual(second_map.number_of_copies, 42)
        self.assertEqual(second_map.notes, 'It is a bit cooler here.')


class ListMapViewTest(TestCase):

    def test_uses_map_list_template(self):
        response = self.client.get('/archives/maps/')
        self.assertTemplateUsed(response, 'archives/map_list.html')

    def test_displays_all_maps(self):
        Map.objects.create(title='first map', origin_date=1951)
        Map.objects.create(title='second map', origin_date=1949)

        response = self.client.get('/archives/maps/')

        self.assertContains(response, 'first map')
        self.assertContains(response, 1951)
        self.assertContains(response, 1949)
        self.assertContains(response, 'second map')


class VideoModelTest(TestCase):

    def test_saving_and_retrieving_video(self):
        some_video = Video()
        some_video.title = 'Some video'
        some_video.description = 'This is just a test video'
        some_video.date = datetime(1955, 11, 12, 22, 4, 00, 000000)
        some_video.url = 'http://mcfly.org/clock-tower-lightning-strike'
        some_video.save()

        saved_videos = Video.objects.all()
        self.assertEqual(saved_videos.count(), 1)

        self.assertEqual(some_video.title, 'Some video')
        self.assertEqual(some_video.description, 'This is just a test video')
        self.assertEqual(some_video.date, datetime(1955, 11, 12, 22, 4, 00, 000000))
        self.assertEqual(some_video.url, 'http://mcfly.org/clock-tower-lightning-strike')