from django.db import models
from django.urls import reverse


class Collection(models.Model):
    """Model representing a Collection of archive categories"""
    title = models.CharField('Title', max_length=30)
    description = models.CharField('Description', max_length=100)
    image = models.URLField('Image URL')
    url = models.CharField('Collection URL', max_length=30)

    class Meta:
        ordering = ['title']

    def __str__(self):
        """
        String for representing the Model object.
        """
        return self.title


class Map(models.Model):
    """
    Model representing a Map
    """
    MAP_TYPE = (
        ('topographical', 'topographical'),
        ('nontopographical', 'nontopographical'),
    )
    map_type = models.CharField('Map Type', choices=MAP_TYPE, max_length=20)
    title = models.CharField('Title', max_length=70)
    location = models.CharField('Location', max_length=50)
    origin_date = models.IntegerField(
        'Origin Date',
        null=True,
        blank=True
    )
    number_of_copies = models.IntegerField('Number of Copies', default=1)
    notes = models.CharField('Notes', max_length=240, blank=True)

    class Meta:
        ordering = ['title', 'origin_date']

    def __str__(self):
        """
        String for representing the Model object.
        """
        return self.title

    def get_absolute_url(self):
        """
        Returns the url to access a detail record for this book.
        """
        return reverse('map-detail', args=[str(self.id)])


class Video(models.Model):
    """Model representing a Video"""
    title = models.CharField('Title', max_length=200)
    subtitle = models.CharField('Subtitle', max_length=200, null=True)
    description = models.TextField('Description', max_length=1000)
    date = models.DateField()
    url = models.URLField('URL')
    image = models.URLField('Image', null=True)

    class Meta:
        ordering = ['date']

    def __str__(self):
        """String for representing the Video object."""
        return self.title

    def get_absolute_url(self):
        """Returns the url to access a detail record for this video."""
        return reverse('video-detail', args=[str(self.id)])
