from django.db import models


class Newsletter(models.Model):
    name = models.CharField(max_length=20)
    date_published = models.DateField(null=True)
    year = models.CharField(max_length=10)
    month = models.CharField(max_length=20)
    url = models.URLField(max_length=200)
    image = models.URLField(max_length=200,blank=True, null=True)

    class Meta:
        ordering = ['-date_published']

    def __str__(self):
        return self.name
