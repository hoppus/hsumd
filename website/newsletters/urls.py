from django.urls import path

from . import views

urlpatterns = [
    path('', views.NewsletterListView.as_view(), name='newsletters'),
]