from django.shortcuts import render
from django.views import generic

from .models import Newsletter

class NewsletterListView(generic.ListView):
    model = Newsletter
    paginate_by = 24