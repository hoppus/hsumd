import datetime

from django.test import TestCase

from newsletters.models import Newsletter


class NewsletterModelTest(TestCase):

    def test_saving_and_retrieving_newsletter(self):
        first_newsletter = Newsletter()
        first_newsletter.name = 'October 1885'
        first_newsletter.date_published = datetime.date(1885, 10, 1)
        first_newsletter.year = '1885'
        first_newsletter.month = '10'
        first_newsletter.url = 'http://gottagetbackintime.com/1885oct.pdf'
        first_newsletter.image = 'http://mcfly.org/1.jpg'
        first_newsletter.save()

        second_newsletter = Newsletter()
        second_newsletter.name = 'November 1955'
        second_newsletter.date_published = datetime.date(1955, 11, 5)
        second_newsletter.year = '1955'
        second_newsletter.month = '11'
        second_newsletter.url = 'http://gottagetbackintime.com/1955nov.pdf'
        second_newsletter.image = 'http://mcfly.org/2.jpg'
        second_newsletter.save()

        saved_newsletters = Newsletter.objects.all()
        self.assertEqual(saved_newsletters.count(), 2)

        self.assertEqual(first_newsletter.name ,'October 1885')
        self.assertEqual(first_newsletter.date_published, datetime.date(1885, 10, 1))
        self.assertEqual(first_newsletter.year ,'1885')
        self.assertEqual(first_newsletter.month, '10')
        self.assertEqual(
            first_newsletter.url,
            'http://gottagetbackintime.com/1885oct.pdf')
        self.assertEqual(first_newsletter.image, 'http://mcfly.org/1.jpg')

        self.assertEqual(second_newsletter.name ,'November 1955')
        self.assertEqual(second_newsletter.date_published, datetime.date(1955,11, 5))
        self.assertEqual(second_newsletter.year ,'1955')
        self.assertEqual(second_newsletter.month, '11')
        self.assertEqual(
            second_newsletter.url,
            'http://gottagetbackintime.com/1955nov.pdf')
        self.assertEqual(first_newsletter.image, 'http://mcfly.org/1.jpg')


class NewsletterViewTest(TestCase):

    def test_uses_archives_template(self):
        response = self.client.get('/newsletters/')
        self.assertTemplateUsed(response, 'newsletters/newsletter_list.html')

    def test_displays_all_items(self):
        first_newsletter = Newsletter()
        first_newsletter.name = 'October 2015'
        first_newsletter.date_published = datetime.date(2015, 10, 21)
        first_newsletter.year = '2015'
        first_newsletter.month = '10'
        first_newsletter.url = 'http://gottagetbackintime.com/2015oct.pdf'
        first_newsletter.save()

        response = self.client.get('/newsletters/')
        self.assertContains(response, 'October 2015')