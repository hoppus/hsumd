from django.shortcuts import redirect, render
from django.views import generic

from .models import Event, Section


def calendar(request):
    # disabled due to COVID-19 return render(request, 'events/calendar.html')
    return redirect('/events/upcoming')


class EventView(generic.ListView):
    model = Event
    paginate_by = 20

    def get_queryset(self):
        keywords = self.request.GET.get('q')
        qs = Event.objects.all()
        if keywords is not None:
            qs = qs.filter(
                Q(name__icontains=keywords) |
                Q(summary__icontains=keywords) 
            )

        return qs


class SectionView(generic.ListView):
    model = Section