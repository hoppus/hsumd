from datetime import datetime

from django.test import TestCase

from events.models import Event, Section


class SectionTest(TestCase):

    def test_saving_and_retrieving_collection(self):
        some_section = Section()
        some_section.title = 'Some section'
        some_section.description = 'This is just a test section'
        some_section.image = 'http://mcfly.org/mcfly-home.png'
        some_section.url = 'things'
        some_section.save()

        saved_sections = Section.objects.all()
        self.assertEqual(saved_sections.count(), 1)

        self.assertEqual(some_section.title, 'Some section')
        self.assertEqual(some_section.description, 'This is just a test section')
        self.assertEqual(some_section.image, 'http://mcfly.org/mcfly-home.png')
        self.assertEqual(some_section.url, 'things')

class EventTest(TestCase):

    def test_saving_and_retrieving_event(self):
        first_event = Event()
        first_event.name = 'Clock Tower Struck by Lightning'
        first_event.date = datetime(1955, 11, 12, 22, 4, 00, 000000)
        first_event.time = datetime(1955, 11, 12, 22, 4, 00, 000000)
        first_event.summary = 'Save the clock tower!'
        first_event.image = 'http://mcfly.org/clocktower.png'
        first_event.save()
        
        saved_events = Event.objects.all()
        self.assertEqual(saved_events.count(), 1)

        self.assertEqual(first_event.name, 'Clock Tower Struck by Lightning')
        self.assertEqual(first_event.date, datetime(1955, 11, 12, 22, 4, 00, 000000))
        self.assertEqual(first_event.time, datetime(1955, 11, 12, 22, 4, 00, 000000))
        self.assertEqual(first_event.summary, 'Save the clock tower!')
        self.assertEqual(first_event.image, 'http://mcfly.org/clocktower.png')

