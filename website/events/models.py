from django.db import models


class Section(models.Model):
    """Model representing a Section of Events"""
    title = models.CharField('Title', max_length=30)
    description = models.CharField('Description', max_length=100)
    image = models.URLField('Image URL')
    url = models.CharField('Section URL', max_length=30)

    class Meta:
        ordering = ['title']

    def __str__(self):
        """String for representing the Model object."""
        return self.title

class Event(models.Model):
    """Model representing an Event"""
    name = models.TextField(max_length=200)
    date = models.DateField(null=True, blank=True)
    time = models.TimeField(null=True, blank=True)
    summary = models.TextField(max_length=800)
    image = models.URLField(max_length=300)
    current = models.BooleanField(default=True)

    class Meta:
        ordering = ['date']

    def __str__(self):
        return self.name

