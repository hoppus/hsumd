from django.urls import path

from . import views

urlpatterns = [
    path('', views.SectionView.as_view(), name='events'),
    path('calendar/', views.calendar, name='calendar'),
    path('upcoming/', views.EventView.as_view(), name='upcoming'),
]

