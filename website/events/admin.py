from django.contrib import admin

from .models import Event, Section


admin.site.register(Event)
admin.site.register(Section)