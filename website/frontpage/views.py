from django.http import HttpResponse
from django.shortcuts import render

from .models import Featurette, ContactCard, AboutUs, Lifemember

# Create your views here.
def index(request):
    aboutus = None
    if len(AboutUs.objects.all()) != 0:
        aboutus = AboutUs.objects.all()[0]

    featurettes = (featurette for featurette in Featurette.objects.order_by('id'))
    context = {
        'aboutus': aboutus,
        'featurettes': featurettes
    }

    return render(request, 'frontpage/index.html', context)

def lifemembers(request):
    ordered_members = (member for member in Lifemember.objects.order_by('last_name'))
    ordered_member_list = list()

    for member in ordered_members:
        ordered_member_list.append(member.__str__())

    context = {
        'members': ordered_member_list
    }

    return render(request, 'frontpage/lifemembers.html', context)

def contactus(request):
    contactcards = (contactcard for contactcard in ContactCard.objects.order_by('id'))
    context = {
        'contactcards': contactcards
    }

    return render(request, 'frontpage/contactus.html', context)
