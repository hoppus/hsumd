# Generated by Django 2.1.5 on 2019-01-26 00:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontpage', '0015_featurette_url_text'),
    ]

    operations = [
        migrations.AlterField(
            model_name='featurette',
            name='image_url',
            field=models.CharField(max_length=200, verbose_name='Image URL'),
        ),
    ]
