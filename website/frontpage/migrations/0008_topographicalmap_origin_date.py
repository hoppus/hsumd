# Generated by Django 2.0.2 on 2018-03-09 20:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontpage', '0007_auto_20180309_2006'),
    ]

    operations = [
        migrations.AddField(
            model_name='topographicalmap',
            name='origin_date',
            field=models.CharField(default="we're in now, now.", max_length=50, verbose_name='Origin Date'),
            preserve_default=False,
        ),
    ]
