from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('lifemembers.html', views.lifemembers, name='lifemembers'),
    path('contactus.html', views.contactus, name='contactus'),
]
