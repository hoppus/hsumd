from django.contrib import admin

from .models import AboutUs, ContactCard, Featurette, Lifemember

# Register your models here.
admin.site.register(Lifemember)
admin.site.register(AboutUs)
admin.site.register(Featurette)
admin.site.register(ContactCard)
