from django.db import models

# Create your models here.
class Lifemember(models.Model):
    first_name = models.CharField('First name (two for couples)', max_length=50)
    last_name = models.CharField(max_length=50)

    def __str__(self):
        return ' '.join((self.first_name, self.last_name))

class AboutUs(models.Model):
    content = models.TextField('About Us',max_length=1500,null=True)


class Featurette(models.Model):
    heading = models.CharField('Heading', max_length=30)
    sub_heading = models.CharField('Subheading', max_length=100)
    image_url = models.CharField('Image URL', max_length=200)
    url_text = models.CharField('URL Text', max_length=200, null=True)
    icon = models.CharField('Icon', max_length=30, null=True)

    def __str__(self):
        return self.heading

class ContactCard(models.Model):
    heading = models.CharField('Heading', max_length=30)
    sub_heading = models.CharField('Subheading', max_length=100)
    icon = models.CharField('Icon', max_length=30, null=True)

    def __str__(self):
        return self.heading
