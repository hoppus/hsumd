upstream app {
    server app:8000;
}

proxy_cache_path /tmp/hsumd-cache/ levels=1:2 keys_zone=hsumd-cache:16m max_size=1g inactive=300m use_temp_path=off;

server {
    listen 80;
    listen 443 default_server ssl;
    charset utf-8;
    server_name hsumd.org www.hsumd.org;
    ssl_certificate     /etc/letsencrypt/live/hsumd.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/hsumd.org/privkey.pem;
    ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers         HIGH:!aNULL:!MD5;

    location / {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://app;
    }

    location /static {
        alias /static;
    }

    location /files/ {
        proxy_pass https://hsumd-public.nyc3.digitaloceanspaces.com/;
        proxy_hide_header Strict-Transport-Security;
        proxy_cache            hsumd-cache;
        proxy_cache_valid      200 60m;
        proxy_cache_use_stale  error timeout updating http_500 http_502 http_503 http_504;
        proxy_cache_revalidate on;
        proxy_cache_lock       on;
        proxy_ignore_headers   Set-Cookie;
        add_header             X-Cache-Status $upstream_cache_status;
    }
}

server {
    charset utf-8;
    server_name high-desert-memories.net high-desert-memories.org;
    location / {
        proxy_pass https://hsumd-public.nyc3.digitaloceanspaces.com/high-desert-memories/;
        proxy_hide_header Strict-Transport-Security;
        proxy_cache            hsumd-cache;
        proxy_cache_valid      200 60m;
        proxy_cache_use_stale  error timeout updating http_500 http_502 http_503 http_504;
        proxy_cache_revalidate on;
        proxy_cache_lock       on;
        proxy_ignore_headers   Set-Cookie;
        add_header             X-Cache-Status $upstream_cache_status;
    }
    location = / {
        rewrite ^ /index.html permanent;
        proxy_hide_header Strict-Transport-Security;
    }
}

