#!/bin/bash

# grab Digital Ocean API token
read -s DOTOKEN < ~/env/dotoken

# create the server
SERVERCREATIONTIME=$(date +%Y%m%d"T"%H%M%S)
MACHINENAME=$(echo prod-$SERVERCREATIONTIME)
docker-machine create --driver digitalocean \
    --digitalocean-image "ubuntu-18-04-x64" \
    --digitalocean-region sfo2 \
    --digitalocean-access-token=$DOTOKEN \
    --digitalocean-monitoring=true $MACHINENAME

# inform user
echo "Production server $MACHINENAME created."
