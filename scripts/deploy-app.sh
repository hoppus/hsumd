#!/bin/bash

# configure environment variables
source .env

# start the containers
docker-compose pull
docker-compose up -d

# copy in nginx config file and static assets
docker cp nginx/default.conf hsumd_nginx_1:/etc/nginx/conf.d/default.conf
docker cp nginx/hsumd_org.crt hsumd_nginx_1:/etc/ssl/certs/
docker cp nginx/hsumd_org.key hsumd_nginx_1:/etc/ssl/certs/
docker cp nginx/staticfiles/admin hsumd_nginx_1:/static/
docker cp nginx/staticfiles/css hsumd_nginx_1:/static/

# restore the database
sleep 5; # give the container enough time to start
dbcontainer=`docker ps | grep hsumd_db | awk '{ print $1 }'`
cat postgres/hsumd.sql | docker exec -i $dbcontainer psql -U $POSTGRES_USER --password

# restart the containers
docker-compose restart
