#!/bin/bash

nginx_container=`docker ps | grep hsumd_nginx | awk '{ print $1 }'`
docker exec -t $nginx_container apt install certbot python-certbot-nginx -y
docker exec -t $nginx_container certbot certonly --nginx -d hsumd.org -m 'admin@hsumd.org' -n --agree-tos

