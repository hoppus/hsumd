#!/bin/bash

# create the server
docker-machine create --driver virtualbox dev

# inform user
echo "dev server created."
