#!/bin/bash

dbcontainer=`docker ps | grep hsumd_db | awk '{ print $1 }'`
docker exec -t $dbcontainer pg_dumpall -c -U postgres > hsumd_`date +%d-%m-%Y"T"%H%M%S`.sql