# HSUMD website
[![pipeline status](https://gitlab.com/hoppus/hsumd/badges/master/pipeline.svg)](https://gitlab.com/hoppus/hsumd/commits/master)

Source code for the hsumd.org website

## Requirements
* A [Linux](https://www.ubuntu.com/desktop) or [Mac](https://www.apple.com/mac/) computer
* [Docker](https://docs.docker.com/docker-for-mac/install/)
* [Digital Ocean personal access token](https://www.digitalocean.com/community/tutorials/how-to-use-the-digitalocean-api-v2#how-to-generate-a-personal-access-token)

## Setup
### Building your Docker server
#### Development

    ./scripts/build-dev-box.sh

#### Production

    ./scripts/build-prod-box.sh

## Deploy Application

    ./scripts/deploy-app.sh

## Run Unit Tests

    ./scripts/run-unit-tests.sh
